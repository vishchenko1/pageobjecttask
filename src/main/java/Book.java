public class Book{
    private String bookName;
    private String author;
    private String price;
    private String  rate;
    private boolean bestSeller;


    void setBookname(String bookname) {
        this.bookName = bookname;
    }
    void setAuthor(String author) {
        this.author = author;
    }
    void setRate(String rate) {
        this.rate = rate;
    }
    void setBestSeller(boolean bestSeller) {
        this.bestSeller = bestSeller;
    }
    void setPrice(String price) { this.price = price; }

    public String getBookname() {
        return bookName;
    }
    public String getAuthor() { return author; }
    public String getRate() { return rate; }
    public boolean isBestSeller() {return bestSeller;  }
    public String getPrice() { return price; }

}
