import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class BaseTest{
private WebDriver driver;
private SearchPage searchpage;
    BaseTest(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com");
        this.driver = driver;
        this.searchpage = new SearchPage(driver);
    }

    SearchPage getsearchpage() { return searchpage; }

    void closeDriver(){driver.quit();}



}
