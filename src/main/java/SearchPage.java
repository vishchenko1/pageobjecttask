import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

class SearchPage{
    private int numberOfElements;
    private String xpathString;
    private static WebDriver driver;
    private List<Book> books = new ArrayList();

    SearchPage(WebDriver driver){ SearchPage.driver = driver; }


    void searchForBooks() {
        WebElement searchbox = driver.findElement(By.id("twotabsearchtextbox"));
        searchbox.sendKeys("Java");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        searchbox.submit();

        numberOfElements = driver.findElements(By.xpath("//div[contains(@class, 's-search-results')]/div")).size();


        for(int i = 1; i<= numberOfElements; i++){

            Book book = new Book();
            xpathString = String.format("//div[contains(@class, 's-search-results')]/div[%d]//h2//span", i);

            book.setBookname(driver.findElement(By.xpath(String.valueOf(xpathString))).getText());


            xpathString = String.format("//div[contains(@class, 's-search-results')]/div[%d]//div[contains(@class, 'a-row a-size-base a-color-secondary')]//a", i);
            try {
                book.setAuthor(driver.findElement(By.xpath(String.valueOf(xpathString))).getText());
            }catch (Exception e){
                book.setAuthor("No author");
            }

            xpathString = String.format("//div[contains(@class, 's-search-results')]/div[%d]//div[contains(@class, 'a-row a-size-small')]/span[1]", i);
            try {
                book.setRate(driver.findElement(By.xpath(String.valueOf(xpathString))).getAttribute("aria-label"));
            }catch (Exception e){
                book.setRate("No rate");
            }

            xpathString = String.format("//div[contains(@class, 's-search-results')]/div[%d]//div[contains(@class, 'sg-row')]", i);
            book.setBestSeller(driver.findElements(By.xpath(String.valueOf(xpathString))).size()!=0);

            xpathString = String.format("//div[contains(@class, 's-search-results')]/div[%d]//a[contains(@class, 'a-size-base a-link-normal s-no-hover a-text-normal')]/span", i);
            try{
                book.setPrice(driver.findElement(By.xpath(String.valueOf(xpathString))).getText());
            }catch (Exception e){
                book.setPrice("No price for Paperback version");
            }



            books.add(book);

        }





    }

    List<Book> getBooks() {
        return books;
    }

}
