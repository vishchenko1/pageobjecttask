import org.testng.Assert;
import org.testng.annotations.Test;




public class SeleniumTask extends BaseTest {


    @Test
    public void test_Java_Books(){

        getsearchpage().searchForBooks();
        closeDriver();

        Assert.assertTrue(getsearchpage().getBooks().stream().anyMatch(Book -> Book.getBookname().equals("Head First Java, 2nd Edition")), "book is not in list");

    }

}

